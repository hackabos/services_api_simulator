const mongoose = require('mongoose');
const User = require('user');
const Invoice = require('invoice');

const connectDb = () => {
  return mongoose.connect(process.env.DATABASE_URL);
};

const models = { User, Invoice };

export { connectDb };
export default models;