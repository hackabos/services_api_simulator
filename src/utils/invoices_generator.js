const mongoose = require('mongoose');
const Invoice = require('../models/invoice.model');
const Product = require('../models/product.model');

exports.getInvoices = (user_id) => {
  const MAX_INVOICES = 10
  const quantity = Math.floor(Math.random() * (MAX_INVOICES - 1)) + 1
  const invoices = []

  for (let i=0; i < quantity; i++) {
    invoices.push(_getInvoice(user_id))
  }

  return invoices
}

const _getInvoice = (user_id) => {
  return new Invoice({
    user_id,
    company_name: _getCompanyNames(),
    company_address: _getAddresses(),
    company_cif: _cifGenerator(),
    client_name: _getClientNames(),
    client_address: _getAddresses(),
    client_cif: _cifGenerator(),
    number: _invoiceNumberGenerator(),
    date: _randomDateGenerator(),
    invoice_url: _getInvoiceFile(),
    products: _getProducts()
  })
}

const _getCompanyNames = () => {
  const names = [
    'Mi tienda de catiuscas',
    'Perico de los Palotes y CIA',
    'Be hipster my friend',
    'Bar celona',
    'Optica Casimiro',
    'Fósforos largos Manuela',
    'El peor negocio del siglo',
    'Banco Cosa Nostra',
    'Jaca vós',
    'Clavos Eustáquio'
  ]

  return _getRandArray(names)
}

const _getAddresses = () => {
  const addresses = [
    'Bellavista 45, 50130 Belchite (Zaragoza)',
    'Reiseñor 17, 46130 Massamagrell (Valencia)',
    'Avenida Cervantes 92, 48300 Gernika-lumo (Biscay)',
    'La Fontanilla 76, 14280 Belalcázar (Córdoba)',
    'C/ Cuesta del Álamo 40, 29611 Istán, (Málaga)',
    'Ctra. de la Puerta 14, 27150 Outeiro De Rei (Lugo)',
    'Caño 53, 33114 Proaza (Asturias)',
    'Boriñaur enparantza 15, 07550 Son Servera (Balearic Islands)',
    'Avda. Alameda Sundheim 17, 22630 Biescas (Huesca)',
    'Av. Santiago Lapuente 60, 50780 Sástago (Zaragoza)'
  ]

  return _getRandArray(addresses)
}

const _getClientNames = () => {
  const names = [
    'Raina Dreda',
    'Wasyl Keara',
    'Árni Marcelino',
    'Heiner Kwadwo',
    'Audo Ewald',
    'August Aleksandrina',
    'Jiang Attila',
    'Parastu Peder',
    'Lessie Theodore',
    'Sierra Pascal'
  ]

  return _getRandArray(names)
}

const _getProducts = () => {
  const MAX_PRODUCTS = 5
  const quantity = Math.floor(Math.random() * (MAX_PRODUCTS - 1)) + 1
  const products = []

  for (let i=0; i < quantity; i++) {
    const MAX_NUM = 500
    const randomnum = Math.floor(Math.random() * (MAX_NUM * 100 - 1 * 100) + 1 * 100) / 100;

    products.push(new Product({
      product: _getProductName(),
      price: randomnum
    }))
  }

  return products
}

const _getProductName = () => {
  const products = [
    'Calcetines rotos, último grito',
    'Libro de recetas de comida basura (Master Chef)',
    'Cuchillas de afeitar desafiladas para no cortarse',
    'Alquiler de servidor Windows 95 x10 años (vintage)',
    'Ruta de senderismo por el Sahara',
    'Vuelos Rihanna Hair (puente aéreo Vigo-Moaña)',
    'Pack de 200 botes de alubias palmeñas jaspeadas',
    'Sesión meditación guiada por Verónica Forqué',
    'Pañuelos de esparto',
    'Discografía completa de La Oreja Baila Sola'
  ]

  return _getRandArray(products)
}

const _getInvoiceFile = () => {
  const INVOICE_FILES = 10
  const number = Math.floor(Math.random() * (INVOICE_FILES - 1)) + 1

  return `${process.env.DOMAIN}/files/invoice_${number}.pdf`
}

const _idGenerator = () => {
  return mongoose.Types.ObjectId();
}

const _invoiceNumberGenerator = () => {
  return Math.floor(Math.random() * 90000000)
}

const _cifGenerator = () => {
  const letters = 'abcdefghijklmnopqrstuvwxyz'.toUpperCase().split('')
  const letter = _getRandArray(letters)

  return Math.floor(10000000 + Math.random() * 90000000) + letter
}

const _randomDateGenerator = () => {
  const start = new Date(2012, 0, 1)
  const end = new Date()

  return new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()));
}

const _getRandArray = array => {
  return array[Math.floor(Math.random() * array.length)]
}
