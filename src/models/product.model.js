const mongoose = require('mongoose');

const ProductSchema = new mongoose.Schema({
  product: {type: String, required: true},
  price: {type: Number, required: true},
  tax_percent: {type: Number},
  quantity: {type: Number, default: 1}
});

module.exports = mongoose.model('Product', ProductSchema);