FROM node:12.14.0

WORKDIR /app

COPY package.json /app

RUN npm install
COPY . /app

USER node

EXPOSE 5000
ENV PORT=5000

CMD npm start