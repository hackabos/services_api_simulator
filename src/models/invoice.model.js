const mongoose = require('mongoose');

const InvoiceSchema = new mongoose.Schema({
  user_id: { type: String, required: true },
  service_id: { type: String, required: true },
  company_name: { type: String },
  company_address: { type: String },
  company_cif: { type: String },
  client_name: { type: String },
  client_address: { type: String },
  client_cif: { type: String },
  number: { type: String, required: true },
  date: { type: Date, required: true },
  products: { type: Array },
  invoice_url: { type: String, required: true }
});

module.exports = mongoose.model('Invoice', InvoiceSchema);