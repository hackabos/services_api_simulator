const jwt = require('jsonwebtoken');
const { getInvoices } = require('../utils/invoices_generator')

const Invoice = require('../models/invoice.model');
const User = require('../models/user.model');
const SECRET_KEY = process.env.JWT_SECRET;

exports.get_invoices = async (req, res) => {
  const token = req.headers.authorization.replace(/bearer/gi, '').trim();
  let payload;
  let invoices;
  
  try {
    payload = jwt.verify(token, SECRET_KEY);
  } catch (error) {
    res.status(401).send(error);
    return;
  }

  try {
    user = await User.findOne({
      _id: payload.userId
    })
  } catch (error) {
    res.status(400).send()
    return
  }

  let finder = {
    user_id: payload.userId
  };

  if('mindate' in req.query || 'maxdate' in req.query) {
    finder.date = {};
  }

  if('mindate' in req.query) {
    finder.date.$gte = parseInt(req.query.mindate);
  }

  if('maxdate' in req.query) {
    finder.date.$lte = parseInt(req.query.maxdate);
  }

  try {
    invoices = await Invoice.find(finder)
  } catch (error) {
    console.log(error)
  }

  res.send(invoices)
}

exports.createDummiInvoices = async (user_id) => {
  const invoices = getInvoices(user_id)

  try {
    await Invoice.collection.insertMany(invoices)
  } catch (error) {
    console.log(error)
  }
}