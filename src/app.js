const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
require('dotenv').config()
const cors = require('cors')
const invoice = require('./routes/invoice.route');
const user = require('./routes/user.route');

app.use(cors())
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use('/files', express.static('files'));

app.use('/invoices', invoice);
app.use('/users', user);

const port = process.env.PORT || 5000
app.listen(port, () => {
  console.log('Server is up and running on port numner ' + port);
});

// Set up mongoose connection
mongoose.connect(process.env.MONGO_URL, { 
  useNewUrlParser: true, 
  useUnifiedTopology: true,
  useCreateIndex: true
});
mongoose.Promise = global.Promise;

let db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));