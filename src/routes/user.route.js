const express = require('express');
const router = express.Router();

const user_controller = require('../controllers/user.controller');

router.post('/setToken', user_controller.set_token);
router.post('/getToken', user_controller.get_token);
router.get('/loginWithToken', user_controller.login_with_token);

module.exports = router;