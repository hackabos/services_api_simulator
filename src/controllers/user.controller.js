const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');

const User = require('../models/user.model');
const { createDummiInvoices } = require('./invoice.controller')

const SECRET_KEY = process.env.JWT_SECRET;

exports.get_token = async (req, res) => {
  const email = req.body.email;
  const password = req.body.password;
  const service = req.body.service;
  let user;

  if (!email || !password || !service) {
    res.status(400).send('invalid arguments');
    return
  }

  try {
    user = await _login(email, password, service)
  } catch (error) {
    console.log(error)
  }

  if (!user) {
    res.status(401).send('Login failed!');
    return;
  }
  
  if (user.token) {
    try {
      jwt.verify(user.token, SECRET_KEY);
      res.status(200).send(user.token);
      return;
    } catch (error) {
      res.status(401).send('Bad token or expired');
      return;
    }
  }else {
    res.status(401).send('No token found');
    return;
  }
}

exports.set_token = async (req, res) => {
  const email = req.body.email;
  const password = req.body.password;
  const service = req.body.service;
  let user;

  if(!email || !password || !service) {
    res.status(400).send('invalid arguments');
    return
  }

  try {
    user = await _login(email, password, service)
  } catch (error) {
    console.log(error)
  }

  if(!user) {
    res.status(401).send('Login failed!');
    return
  }

  const expiresIn = 24 * 60 * 60; // 24 horas
  const accessToken = jwt.sign({
    userId: user.id,
    service
  }, SECRET_KEY, {
    expiresIn: expiresIn
  });

  try {
    await _saveToken(user, accessToken);
  } catch (error) {
    res.status(400).send('The token can not be saved')
    return
  }

  res.status(200).send({'access_token': accessToken, 'expires_in': expiresIn});
}

const _saveToken = async (user, token) => {
  user.token = token

  return user.save();
}

const _login = async (email, password, service) => {
  let user;

  try {
    user = await User.findOne({email, service})
    if (!user) {
      user = await _setupDummiData(email, password, service)
    }
  } catch (error) {
    console.log(error)
    throw new Error()
  }

  try {
    passMatches = await bcrypt.compare(password, user.password)
  } catch (error) {
    console.log(error)
  }
  
  return passMatches && user;
}

const _setupDummiData = async (email, password, service) => {
  let user
  try {
    user = await _createUser(email, password, service)
    await createDummiInvoices(user.id)
  } catch (error) {
    throw new Error()
  }

  return user
}

const _createUser = async (email, password, service) => {
  password = bcrypt.hashSync(password);
  let user;

  try {
    user = await User.create({email, password, service})
  } catch (error) {
    console.log(error)
    throw new Error()
  }
      
  return user;
}

exports.login_with_token = async (req, res) => {
  const token = req.headers.authorization.replace(/bearer/gi, '').trim();
  let decoded

  try {
    decoded = await jwt.verify(token, SECRET_KEY)
  } catch (error) {
    res.status(401).json()
    return
  }

  res.json(decoded)
}