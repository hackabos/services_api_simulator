const express = require('express');
const router = express.Router();

// const { jwtAuth } = require('../middlewares/index')
const invoice_controller = require('../controllers/invoice.controller');

router.get('/get', invoice_controller.get_invoices);

module.exports = router;